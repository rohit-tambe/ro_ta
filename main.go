package main

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/rohit-tambe/login-process/database"

	"bitbucket.org/rohit-tambe/login-process/service"
	"bitbucket.org/rohit-tambe/login-process/view"
)

// MyResponseWriter custom response
type MyResponseWriter struct {
	http.ResponseWriter
	code int
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	service.IndexTemplate().Execute(w, nil)
}

func requestMiddleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%v  and %v", r.Host, r.URL)
		log.Printf("Got a %s request for: %v", r.Method, r.URL)
		myrw := &MyResponseWriter{ResponseWriter: w}
		handler.ServeHTTP(myrw, r)
	})
}

func checkCookieMiddleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(r.URL.Path)
		if ("/updateUser" == r.URL.Path) || ("/userRagistration" == r.URL.Path) {
			cookie, err := r.Cookie("key")
			if err != nil {
				log.Println("in cookie middleware ", err.Error())
				service.IndexTemplate().Execute(w, nil)
			} else {
				log.Println("Cookie ", cookie)
				handler.ServeHTTP(w, r)
			}
		} else if "/googleLogin" == r.URL.Path {
			cookie, err := r.Cookie("email")
			if err != nil {
				log.Println("in cookie middleware ", err.Error())
				service.IndexTemplate().Execute(w, nil)
			} else {
				handler.ServeHTTP(w, r)
				log.Println("Cookie ", cookie)
			}
		} else {
			handler.ServeHTTP(w, r)
		}
	})
}
func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", indexHandler)
	mux.HandleFunc("/login", view.Login)
	mux.HandleFunc("/userRagistration", view.ProfileCompletionForm)
	mux.HandleFunc("/updateUser", view.UpdateUser)
	mux.HandleFunc("/googleLogin", view.GoogleLogin)
	mux.HandleFunc("/sendPassword", view.SendPasswordLink)
	mux.HandleFunc("/forgotPassword", view.ForgotPassword)
	mux.HandleFunc("/resetPassword", view.ResetPassword)
	fmt.Println("server listen in port 9999 ...")
	wrapMux := requestMiddleware(mux)
	wrapMux = checkCookieMiddleware(mux)
	http.ListenAndServe(":9999", wrapMux)
	defer database.GetDB().Close()
}

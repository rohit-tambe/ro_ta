package model

import (
	"errors"
	"fmt"
	"log"
	"time"

	"bitbucket.org/rohit-tambe/login-process/database"
	uuid "github.com/gofrs/uuid"
	"github.com/lib/pq"
)

// Users user details
type Users struct {
	Key          string    `json:"key"`
	Telephone    string    `json:"telephone"`
	Address      string    `json:"address"`
	FullName     string    `json:"full_name"`
	Email        string    `json:"email"`
	IsGmailLogin bool      `json:"is_gmail_login"`
	Password     string    `json:"password"`
	CreatedAt    time.Time `json:"created_at"`
	UpdateAt     time.Time `json:"updated_at"`
}

// InsertWithManualSignUp insert user Details
func InsertWithManualSignUp(email, password string) (string, error) {
	key := GenerateUUID()
	inserUser := fmt.Sprintf(`INSERT INTO users(key, email, password,is_gmail_login,telephone,full_name,address)
	VALUES('%s', '%s', '%s',false,'','','') RETURNING key`, key, email, password)
	err := database.GetDB().QueryRow(inserUser).Scan(&key)
	if err != nil {
		if pgErr, ok := err.(*pq.Error); ok {
			if pgErr.Code == "23505" {
				return "", errors.New("user aleardy exist")
			}
		}
	}

	log.Printf("%+v\n", key)
	return key, err
}

// InsertWithGoogleSignUp insert user for google signup
func InsertWithGoogleSignUp(email, name string) (Users, error) {
	key := GenerateUUID()
	inserUser := fmt.Sprintf(`INSERT INTO users(key, email, password,is_gmail_login,telephone,full_name,address)
	VALUES('%s', '%s', '%s',true,'','%s','') RETURNING key,full_name`, key, email, email, name)
	err := database.GetDB().QueryRow(inserUser).Scan(&key, &name)
	u := Users{Key: key, Email: email, FullName: name}
	log.Printf("%+v\n", u)
	return u, err
}

// GetUserByKey insert user Details
func GetUserByKey(key string) (Users, error) {
	var user Users
	selectUser := "SELECT key,telephone,address,full_name,email FROM users WHERE key=$1"
	log.Println(selectUser)
	err := database.GetDB().QueryRow(selectUser, key).Scan(&user.Key, &user.Telephone, &user.Address,
		&user.FullName, &user.Email)
	return user, err
}

// GetUserByEmail get user details by email id
func GetUserByEmail(email string) (Users, error) {
	var user Users
	selectUser := "SELECT key,telephone,address,full_name,email FROM users WHERE email=$1"
	log.Println(selectUser)
	err := database.GetDB().QueryRow(selectUser, email).Scan(&user.Key, &user.Telephone, &user.Address,
		&user.FullName, &user.Email)
	return user, err
}

// GetUserByEmailAndPassword get user details
func GetUserByEmailAndPassword(email, password string) (Users, error) {
	var user Users
	selectUser := "SELECT key,telephone,address,full_name,email FROM users WHERE email=$1 and password=$2"
	log.Println(selectUser)
	err := database.GetDB().QueryRow(selectUser, email, password).Scan(&user.Key, &user.Telephone, &user.Address,
		&user.FullName, &user.Email)
	return user, err
}

// UpdateUserInformation update user information
func (u *Users) UpdateUserInformation(key string) (string, error) {
	updateUser := fmt.Sprintf(`
	UPDATE users SET telephone = '%s', 
	address = '%s',full_name = '%s'  where key = '%s' RETURNING key,email;`,
		u.Telephone, u.Address, u.FullName, key)
	log.Println(updateUser)
	var email string
	err := database.GetDB().QueryRow(updateUser).Scan(&key, &email)
	if err != nil {
		if pgErr, ok := err.(*pq.Error); ok {
			if pgErr.Code == "23505" {
				return "", errors.New("user aleardy exist")
			}
		}
	}
	u.Email = email
	u.Key = key
	log.Printf("%+v\n", key)
	return key, err
}

// UpdateUserProfile update user profile information
func (u *Users) UpdateUserProfile(key string) (string, error) {
	updateUser := fmt.Sprintf(`
	UPDATE users SET telephone = '%s', 
	address = '%s',full_name = '%s',email = '%s'  where key = '%s' RETURNING key,email;`,
		u.Telephone, u.Address, u.FullName, u.Email, key)
	log.Println(updateUser)
	var email string
	err := database.GetDB().QueryRow(updateUser).Scan(&key, &email)
	if err != nil {
		if pgErr, ok := err.(*pq.Error); ok {
			if pgErr.Code == "23505" {
				return "", errors.New("user aleardy exist")
			}
		}
	}
	u.Email = email
	u.Key = key
	log.Printf("%+v\n", key)
	return key, err
}

// UpdateUserPassword update user profile information
func UpdateUserPassword(password, key string) (string, error) {
	updateUser := fmt.Sprintf(`
	UPDATE users SET password = '%s' 
	where key = '%s' RETURNING key;`,
		password, key)
	log.Println(updateUser)
	err := database.GetDB().QueryRow(updateUser).Scan(&key)
	log.Printf("%+v\n", key)
	return key, err
}

// GenerateUUID for key
func GenerateUUID() string {
	uuid, err := uuid.NewV1()
	if err != nil {
		log.Println(err)
	}
	return uuid.String()
}

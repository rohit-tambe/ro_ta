package model

import (
	"fmt"
	"testing"
)

func TestInsertWithManualSignUp(t *testing.T) {
	if key, err := InsertWithManualSignUp("oo1@gmail.com", "rohit"); err != nil {
		t.Errorf("%v", err)
	} else {
		fmt.Println(key)
	}
}

func TestUsers_UpdateUserInformation(t *testing.T) {
	key := "006b7851-c908-11ea-bcc1-f07959335e14"
	u := Users{Telephone: "9763951288", FullName: "Jani Tambe", Address: "pune"}
	got, err := u.UpdateUserInformation(key)
	if err != nil {
		t.Errorf("Users.UpdateUserInformation() = %v, want %v", err, key)
	}
	if got != key {
		t.Errorf("Users.UpdateUserInformation() = %v, want %v", got, key)
	}
	fmt.Println(got, u.Email)
}

func TestGetUserByKey(t *testing.T) {
	got, err := GetUserByKey("006b7851-c908-11ea-bcc1-f07959335e14")
	if err != nil {
		t.Errorf("GetUserByKey() error = %v", err)
	}
	t.Logf("%#v", got)
}

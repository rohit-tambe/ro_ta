FROM golang:1.13 as build

ENV GO111MODULE=on

WORKDIR /app

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o main .

# stage 2 pack
FROM alpine:latest
ENV HOSTNAME www.myhost.com
# RUN ping www.myhost.com
COPY --from=build /app/main /

# add web folder
RUN mkdir template
COPY template /template

EXPOSE 9999

ENTRYPOINT ["/main"]
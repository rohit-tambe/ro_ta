﻿CREATE TABLE users
(
  key uuid NOT NULL,
  telephone  text ,
  address text ,
  full_name text ,
  email text NOT NULL,
  password text NOT NULL,
  is_gmail_login boolean,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_at timestamp with time zone NOT NULL DEFAULT now(),
  CONSTRAINT users_pkey PRIMARY KEY (key),
  unique(email)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO postgres;


CREATE INDEX ON "users" ("email","password");


module bitbucket.org/rohit-tambe/login-process

go 1.13

require (
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/lib/pq v1.7.0
	github.com/satori/go.uuid v1.2.0
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)

package view

import (
	"net/http"

	"bitbucket.org/rohit-tambe/login-process/model"
	"bitbucket.org/rohit-tambe/login-process/service"
)

// ProfileCompletionForm get user details and save
func ProfileCompletionForm(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	user := model.Users{Telephone: r.FormValue("telephone"), FullName: r.FormValue("fullName"), Address: r.FormValue("address")}
	_, err := user.UpdateUserInformation(r.PostFormValue("key"))
	if err != nil {
		service.ProfileCompletionTemplate().Execute(w, Response{"Failed", err.Error()})
	} else {
		userResponse := UserResponse{user, Response{}}
		service.ProfilePageTemplate().Execute(w, userResponse)
	}
}

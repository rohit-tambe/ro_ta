package view

import (
	"net/http"

	"bitbucket.org/rohit-tambe/login-process/model"
	"bitbucket.org/rohit-tambe/login-process/service"
)

// UpdateUser update user profile
func UpdateUser(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	user := model.Users{Key: r.PostFormValue("key"), Telephone: r.FormValue("telephone"),
		FullName: r.FormValue("fullName"), Address: r.FormValue("address"), Email: r.FormValue("email")}
	_, err := user.UpdateUserProfile(user.Key)
	if err != nil {
		userResponse := UserResponse{user, Response{"Failed", err.Error()}}
		service.ProfilePageTemplate().Execute(w, userResponse)
	} else {
		userResponse := UserResponse{user, Response{"Successful", "update successfully"}}
		service.ProfilePageTemplate().Execute(w, userResponse)
	}
}

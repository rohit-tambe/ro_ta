package view

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"bitbucket.org/rohit-tambe/login-process/model"
	"bitbucket.org/rohit-tambe/login-process/service"
)

// GoogleLogin user sign through google auth
func GoogleLogin(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	fmt.Printf("%+v\n", r.Form)
	u, err := model.GetUserByEmail(r.FormValue("email"))
	if err != nil {
		log.Println("in login err ", err.Error())
		u, err := model.InsertWithGoogleSignUp(r.FormValue("email"), r.FormValue("name"))
		if err != nil {
			service.IndexTemplate().Execute(w, Response{"Failed", err.Error()})
		} else {
			expiration := time.Now().Add(1 * 24 * time.Hour)
			cookie := http.Cookie{Name: "key", Value: u.Key, Expires: expiration}
			http.SetCookie(w, &cookie)
			service.ProfileCompletionTemplate().Execute(w, u)
		}
	} else {
		userResponse := UserResponse{u, Response{}}
		expiration := time.Now().Add(1 * 24 * time.Hour)
		cookie := http.Cookie{Name: "key", Value: u.Key, Expires: expiration}
		http.SetCookie(w, &cookie)
		service.ProfilePageTemplate().Execute(w, userResponse)
	}
}

package view

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/rohit-tambe/login-process/model"
	"bitbucket.org/rohit-tambe/login-process/service"
)

// SendPasswordLink send reset password link
func SendPasswordLink(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()
	fmt.Printf("%+v\n", r.Form)
	email := r.FormValue("email")
	u, err := model.GetUserByEmail(email)
	if err != nil {
		log.Println("in login err ", err.Error())
		if len(email) == 0 {
			service.ForgotPasswordTemplate().Execute(w, "Please enter your email")
		} else {
			service.ForgotPasswordTemplate().Execute(w, "user not exist")
		}
	} else {
		go service.SendResetPasswordEmail(service.EmailContent{r.Host, r.URL, u.Key, u.Email})
		service.ForgotPasswordTemplate().Execute(w, "please check your email")
	}
}

package view

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"bitbucket.org/rohit-tambe/login-process/model"
	"bitbucket.org/rohit-tambe/login-process/service"
)

// Response parameters
type Response struct {
	Status string
	Reason string
}

// UserResponse profile template
type UserResponse struct {
	User     model.Users
	Response Response
}

// Login user
func Login(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	fmt.Printf("%+v\n", r.Form)
	email := r.FormValue("email")
	password := r.FormValue("password")
	if (len(email) == 0) && (len(password) == 0) {
		service.IndexTemplate().Execute(w, Response{"Failed", "Please enter email and password"})
		return
	}
	u, err := model.GetUserByEmailAndPassword(email, password)
	if err != nil {
		log.Println("in login err ", err.Error())
		key, err := model.InsertWithManualSignUp(email, password)
		if err != nil {
			service.IndexTemplate().Execute(w, Response{"Failed", err.Error()})
		} else {
			u.Key = key
			expiration := time.Now().Add(1 * 24 * time.Hour)
			cookie := http.Cookie{Name: "key", Value: u.Key, Expires: expiration}
			http.SetCookie(w, &cookie)
			service.ProfileCompletionTemplate().Execute(w, u)
		}

	} else {
		expiration := time.Now().Add(1 * 24 * time.Hour)
		cookie := http.Cookie{Name: "key", Value: u.Key, Expires: expiration}
		http.SetCookie(w, &cookie)
		userResponse := UserResponse{u, Response{}}
		service.ProfilePageTemplate().Execute(w, userResponse)
	}
}

package view

import (
	"fmt"
	"net/http"

	"bitbucket.org/rohit-tambe/login-process/model"
	"bitbucket.org/rohit-tambe/login-process/service"
)

// ResetPassword reset user password
func ResetPassword(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()
	fmt.Printf("%+v\n", r.Form)
	key := r.PostFormValue("key")
	password := r.FormValue("password")
	_, err := model.UpdateUserPassword(password, key)
	if err != nil {
		service.ResetPasswordTemplate().Execute(w, UserResponse{User: model.Users{Key: key},
			Response: Response{Status: "Failed", Reason: err.Error()}})
	} else {
		service.ResetPasswordTemplate().Execute(w, UserResponse{User: model.Users{Key: key},
			Response: Response{Status: "Successful", Reason: "password reset . please login again"}})
	}
}

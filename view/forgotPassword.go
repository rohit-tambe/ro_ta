package view

import (
	"log"
	"net/http"

	"bitbucket.org/rohit-tambe/login-process/model"
	"bitbucket.org/rohit-tambe/login-process/service"
)

// ForgotPassword recieve forgot password link and render reset password screen
func ForgotPassword(w http.ResponseWriter, r *http.Request) {

	key := r.URL.Query().Get("token")
	if key != "" {
		log.Println("in forgot password", key)
		service.ResetPasswordTemplate().Execute(w, UserResponse{User: model.Users{Key: key},
			Response: Response{Status: "", Reason: ""}})

	} else {
		service.ForgotPasswordTemplate().Execute(w, nil)
	}
}

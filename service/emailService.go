package service

import (
	"fmt"
	"log"
	"net/url"

	"gopkg.in/gomail.v2"
)

// EmailContent send email coctent
type EmailContent struct {
	Host        string
	URL         *url.URL
	Key         string
	SenderEmail string
}

// SendResetPasswordEmail send email for forgot password
func SendResetPasswordEmail(emailContent EmailContent) error {
	mailID := "rohit.tambe88@gmail.com"
	mail := gomail.NewMessage()
	mail.SetHeader("From", mailID)
	mail.SetHeader("To", emailContent.SenderEmail)
	mail.SetHeader("Subject", "Password reset assistance")
	mail.SetBody("text/html", getEmailTemplate(emailContent))
	dailer := gomail.NewPlainDialer("smtp.gmail.com", 587, mailID, "*****")

	if err := dailer.DialAndSend(mail); err != nil {
		log.Println(err)
	}
	fmt.Println("email sent")
	return nil
}
func getEmailTemplate(e EmailContent) string {
	templ := fmt.Sprintf(`
	<html>
	<body>
	<h3>We received a request to reset the password</h3>
	<h4>Click on the link below to reset your password</h4>
	http://%s/forgotPassword?token=%s
	</body>
	</html>
	`, e.Host, e.Key)
	return templ
}

package service

import "html/template"

func IndexTemplate() *template.Template {
	tmpl := template.Must(template.ParseFiles("template/index.html"))
	return tmpl
}
func ProfilePageTemplate() *template.Template {
	tmpl := template.Must(template.ParseFiles("template/profilePage.html"))
	return tmpl
}
func ProfileCompletionTemplate() *template.Template {
	tmpl := template.Must(template.ParseFiles("template/profileCompletionForm.html"))
	return tmpl
}
func ForgotPasswordTemplate() *template.Template {
	tmpl := template.Must(template.ParseFiles("template/forgotPassword.html"))
	return tmpl
}
func ResetPasswordTemplate() *template.Template {
	tmpl := template.Must(template.ParseFiles("template/resetPassword.html"))
	return tmpl
}

package database

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq"
)

var db *sql.DB

func init() {

	// db, err := sql.Open("mysql", "postgres:root@tcp(localhost:3306)/login_process")

	psqlInfo := "user=postgres password=root host=localhost port=5432 dbname=login_process sslmode=disable"
	con, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		log.Fatal(err.Error())
	}
	db = con

	// if there is an error opening the connection, handle it
	if err != nil {
		panic(err.Error())
	}

}

// GetDB return database connection
func GetDB() *sql.DB {
	return db
}
